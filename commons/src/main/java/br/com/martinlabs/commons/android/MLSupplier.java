package br.com.martinlabs.commons.android;

/**
 * Created by gil on 3/17/16.
 */
public interface MLSupplier<T> {

     T get();
}
