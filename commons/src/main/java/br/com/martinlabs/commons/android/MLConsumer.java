package br.com.martinlabs.commons.android;

/**
 * Created by gil on 18/01/16.
 */
public interface MLConsumer<T> {

    void accept(T t);

}
