package br.com.martinlabs.commons.android;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by gil on 3/7/16.
 */
public class Req2 {

    private static String  dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ";

    public static HttpRequest post(String url, Object param){
        try {
            Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
            String paramStr = null;

            if (param != null) {
                paramStr = gson.toJson(param);
            }

            HttpRequest req = HttpRequest.post(url);
            req.header("Accept-Language","pt");
            req.contentType("application/json");

            if (paramStr != null) {
                req.send(paramStr);
            }

            return req;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static HttpRequest get(String url, Object... params){
        try {
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof Date) {
                    params[i] = new SimpleDateFormat(dateFormat).format((Date) params[i]);
                }
            }

            HttpRequest req = HttpRequest.get(url, true, params);

            return req;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T post(String url, Object param, Class<T> classOfT) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = post(url, param);

        if (req == null) {
            return null;
        }

        return convertResponse(classOfT, gson, req);
    }

    private static <T> T convertResponse(Class<T> classOfT, Gson gson, HttpRequest req) {

        try {
            String body = req.body();
            if (req.ok()) {
                T jsonResult = gson.fromJson(body, classOfT);
                return jsonResult;
            } else if (body != null) {
                throw gson.fromJson(body, RespException.class);
            } else {
                return null;
            }
        }

        catch (HttpRequest.HttpRequestException  e)
        {
            return null;
        }
        catch (IllegalStateException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (JsonSyntaxException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    private static <T> T convertResponse(Type typeOfT, Gson gson, HttpRequest req) {
        try {
            String body = req.body();
            if (req.ok()) {
                T jsonResult = gson.fromJson(body, typeOfT);
                return jsonResult;
            } else if (body != null && !body.isEmpty()) {
                throw gson.fromJson(body, RespException.class);
            } else {
                return null;
            }
        }catch (HttpRequest.HttpRequestException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (IllegalStateException e)
        {
            e.printStackTrace();
            return null;
        }
        catch (JsonSyntaxException e)
        {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T post(String url, Object param, Type typeOfT) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = post(url, param);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static <T> T post(String url, Type typeOfT, Object... params) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();

        if (params.length % 2 != 0) {
            return null;
        }

        HashMap<String, Object> param = new HashMap<>();

        for (int i = 0; i < params.length -1; i += 2) {
            param.put((String)params[i], params[i+1]);
        }

        HttpRequest req = post(url, param);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static <T> T get(String url, Class<T> classOfT, Object... params) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = get(url, params);

        if (req == null) {
            return null;
        }

        return convertResponse(classOfT, gson, req);
    }

    public static <T> T get(String url, Type typeOfT, Object... params) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = get(url, params);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static <T> T put(String url, Type typeOfT, Object... params) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = put(url, params);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static HttpRequest put(String url, Object... params){
        try {
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof Date) {
                    params[i] = new SimpleDateFormat(dateFormat).format((Date) params[i]);
                }
            }

            HttpRequest req = HttpRequest.put(url, true, params);

            return req;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T delete(String url, Type typeOfT, Object... params) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = delete(url, params);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static HttpRequest delete(String url, Object... params){
        try {
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof Date) {
                    params[i] = new SimpleDateFormat(dateFormat).format((Date) params[i]);
                }
            }

            HttpRequest req = HttpRequest.delete(url, true, params);

            return req;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T getWithAuthorization(String url, Type typeOfT,String authValue ,Object... params) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = getWithAuth(url,authValue, params);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static <T> T getWithAuthorization(String url, Class<T> classOfT, String authValue, Object... params) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = getWithAuth(url, authValue, params);

        if (req == null) {
            return null;
        }

        return convertResponse(classOfT, gson, req);
    }

    public static HttpRequest getWithAuth(String url, String authValue, Object... params){
        try {
            for (int i = 0; i < params.length; i++) {
                if (params[i] instanceof Date) {
                    params[i] = new SimpleDateFormat(dateFormat).format((Date) params[i]);
                }
            }

            HttpRequest req = HttpRequest.get(url, true, params).header("Authorization","Bearer " + authValue);
            req.header("Accept-Language","pt");

            return req;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T postWithAuthorization(String url, String authValue, Object param, Type typeOfT) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = postWithAuthorization(url, authValue, param);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static <T> T postWithAuthorization(String url, String authValue, Type typeOfT) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
        HttpRequest req = postWithAuthorization(url, authValue);

        if (req == null) {
            return null;
        }

        return convertResponse(typeOfT, gson, req);
    }

    public static HttpRequest postWithAuthorization(String url, String authValue, Object param){
        try {
            Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();
            String paramStr = null;

            if (param != null) {
                paramStr = gson.toJson(param);
            }

            HttpRequest req = HttpRequest.post(url).header("Authorization","Bearer " + authValue);;
            req.contentType("application/json","UTF-8");
            req.header("Accept-Language","pt");

            if (paramStr != null) {
                req.send(paramStr);
            }

            return req;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public static HttpRequest postWithAuthorization(String url, String authValue){
        try {

            HttpRequest req = HttpRequest.post(url).header("Authorization","Bearer " + authValue);;
            req.contentType("application/json","UTF-8");
            req.header("Accept-Language","pt");

           return req;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> T postWithAuthorization(String url, String authValue, Object param, Class<T> classOfT) {
        Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();

        HttpRequest req = postWithAuthorization(url, authValue);

        if (req == null) {
            return null;
        }

        return convertResponse(classOfT, gson, req);
    }

    public static int test() {
        return HttpRequest.get("http://google.com").code();
    }


}
