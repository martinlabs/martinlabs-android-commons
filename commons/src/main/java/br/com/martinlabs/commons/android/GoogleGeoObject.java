package br.com.martinlabs.commons.android;

import java.util.List;

/**
 * Created by developer on 11/27/17.
 */

public class GoogleGeoObject {

    private List<GoogleResult> results;

    public List<GoogleResult> getResults() {
        return results;
    }

    public void setResults(List<GoogleResult> results) {
        this.results = results;
    }

    public class GoogleResult
    {
        private GoogleGeometry geometry;
        private List<GoogleAddress> address_components;



        public GoogleGeometry getGeometry() {
            return geometry;
        }

        public void setGeometry(GoogleGeometry geometry) {
            this.geometry = geometry;
        }

        public List<GoogleAddress> getAddress() {
            return address_components;
        }

        public void setAddress(List<GoogleAddress> address_components) {
            this.address_components = address_components;
        }
    }

    public class GoogleAddress
    {
        private String long_name;
        private String short_name;
        private List<String> types;


        public String getLong_name() {
            return long_name;
        }

        public void setLong_name(String long_name) {
            this.long_name = long_name;
        }

        public String getShort_name() {
            return short_name;
        }

        public void setShort_name(String short_name) {
            this.short_name = short_name;
        }

        public List<String> getSublocality() {
            return types;
        }

        public void setSublocality(List<String> sublocality) {
            this.types = sublocality;
        }
    }

    public class GoogleGeometry
    {
        private String location_type;
        private GoogleLocation location;

        public String getLocation_type() {
            return location_type;
        }

        public void setLocation_type(String location_type) {
            this.location_type = location_type;
        }

        public GoogleLocation getLocation() {
            return location;
        }

        public void setLocation(GoogleLocation location) {
            this.location = location;
        }
    }

    public class GoogleLocation
    {
        private double lat;
        private double lng;

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }
    }
}
