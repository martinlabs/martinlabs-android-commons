package br.com.martinlabs.commons.android;

/**
 * Created by gil on 8/31/16.
 */
public interface FragmentLifecycle {

    void onPauseFragment();
    void onResumeFragment();

}
