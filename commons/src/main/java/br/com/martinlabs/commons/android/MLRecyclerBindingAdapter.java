package br.com.martinlabs.commons.android;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
/**
 * Created by developer on 7/25/17.
 */


public class MLRecyclerBindingAdapter extends RecyclerView.Adapter<MLRecyclerBindingAdapter.DataViewHolder> {

    private Context context;
    private int itemCount;
    private ViewHolderBinder binder;
    private int layoutRes;
    private RecyclerItemClickListener clickListener;

    public MLRecyclerBindingAdapter(Context context, int layoutRes,ViewHolderBinder binder)
    {
        this.context = context;
        this.binder = binder;
        this.layoutRes = layoutRes;
    }

    public interface RecyclerItemClickListener {
        void onClick(View v, int position);
    }

    public interface ViewHolderBinder {
        void onBindViewHolder(DataViewHolder holder, int position);
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
        notifyDataSetChanged();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),layoutRes,parent,false);

        return new DataViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        binder.onBindViewHolder(holder, position);

    }

    @Override
    public int getItemCount() {
        return this.itemCount;
    }

    public class DataViewHolder extends RecyclerView.ViewHolder {


        ViewDataBinding vwDatabinding;

        public DataViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.vwDatabinding = binding;

        }

        public void bind(Object obj, int idBr)
        {
            this.vwDatabinding.setVariable(idBr,obj);

        }

        public ViewDataBinding getBinding()
        {
            return vwDatabinding;

        }
    }
}
