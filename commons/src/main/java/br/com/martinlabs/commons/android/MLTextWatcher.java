package br.com.martinlabs.commons.android;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

/**
 * Created by developer on 11/9/17.
 */

public class MLTextWatcher implements TextWatcher {

    private View view;
    private Runnable runnable;

    public MLTextWatcher(View view, Runnable runnable) {
        this.view = view;
        this.runnable = runnable;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
    }

    public void afterTextChanged(Editable editable) {
        runnable.run();
    }
}
