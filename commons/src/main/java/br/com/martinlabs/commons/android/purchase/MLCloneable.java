package br.com.martinlabs.commons.android.purchase;

/**
 * Created by gil on 9/28/16.
 */

public interface MLCloneable extends Cloneable {

    public Object clone() throws CloneNotSupportedException;

}
